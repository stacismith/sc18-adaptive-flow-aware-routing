import copy
from collections import defaultdict


# Computes link loads given the routing table, traffic matrix, and topology.
def calculate_loads(rtable, tm, nodeinfo, connections):
  print "Calculating loads given current rtable..."

  linkloads = defaultdict(int)
  dests = defaultdict(list)  # destination nodes for active streams on link

  # Iterate over streams in the traffic matrix.
  for src, dest, weight in tm:
    srcswitch = nodeinfo[src]['switch']
    destswitch = nodeinfo[dest]['switch']
 
    # Using the routing table and topology, trace the stream through switches/links.
    destlid = nodeinfo[dest]['lid']
    currswitch = srcswitch
    while currswitch != destswitch:
      # Look up next hop port.
      port = rtable[currswitch][str(destlid)]

      # Increment the load on the outgoing link.
      linkloads[(currswitch, port)] += weight
      dests[(currswitch, port)].append(str(destlid))

      nextswitch = connections[(currswitch, port)][0]
      currswitch = nextswitch

  return linkloads, dests


# Runs AFAR algorithm as described in SC'18 paper.
def get_AFAR_rtable_by_max_load(cutoff, maxiters, rtable, tm, nodeinfo, connections):
  print "Modifying routing..."

  # Do AFAR algorithm, returning new modified copy of rtable.
  newrtable = copy.deepcopy(rtable)
  linkloads, dests = calculate_loads(rtable, tm, nodeinfo, connections)

  # Try changing up to iters links.
  for i in range(maxiters):
    maxlink = max(linkloads, key=lambda x: linkloads[x])
    destlids = dests[maxlink]

    if linkloads[maxlink] <= cutoff:
      print "Achieved link load bound of %d GB after %d fixes." % (cutoff / int(1e9), i)
      break

    # Modify routing tables to send messages for first destlid somewhere else.
    destlid = destlids[0]
    table = newrtable[maxlink[0]]

    problemport = int(table[destlid])
    # Only make adjustment if going up -- going down paths are limited.
    # TODO: allow re-routing going down as well
    # TODO: special case if this is a top-level switch, which has no up ports
    if int(problemport) > 18:
      # Choose best up port and re-route in table entry.
      newport = min(range(19, 37), key=lambda x: linkloads[(maxlink[0], str(x))])
      table[destlid] = str(newport)

    linkloads, dests = calculate_loads(newrtable, tm, nodeinfo, connections)

  return newrtable


# Runs AFAR algorithm for a specified number of iterations.
def get_AFAR_rtable_by_iters(iters, rtable, tm, nodeinfo, connections):
  print "Modifying routing..."

  # Do AFAR algorithm, returning new modified copy of rtable.
  newrtable = copy.deepcopy(rtable)
  linkloads, dests = calculate_loads(rtable, tm, nodeinfo, connections)

  # Try changing up to iters links.
  for i in range(iters):
    maxlink = max(linkloads, key=lambda x: linkloads[x])
    destlids = dests[maxlink]

    # Modify routing tables to send messages for first destlid somewhere else.
    destlid = destlids[0]
    table = newrtable[maxlink[0]]

    problemport = int(table[destlid])
    # Only make adjustment if going up -- going down paths are limited.
    # TODO: allow re-routing going down as well
    # TODO: special case if this is a top-level switch, which has no up ports
    if int(problemport) > 18:
      # Choose best up port and re-route in table entry.
      newport = min(range(19, 37), key=lambda x: linkloads[(maxlink[0], str(x))])
      table[destlid] = str(newport)

    linkloads, dests = calculate_loads(newrtable, tm, nodeinfo, connections)

  return newrtable
