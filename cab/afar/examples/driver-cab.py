import sys
import numpy as np

import routing_routines as rt
import parsers


# Usage: python driver-cab.py <network topology file> <routing table file> <traffix matrix file> <new routing table output file> [--load <max link load GB> | --fixes <max routing table entries changed>]
if __name__ == "__main__":
  if (len(sys.argv) != 7):
    print "Usage: python", sys.argv[0], "<network topology file> <routing table file> <traffix matrix file> <new routing table output file> [--load <max link load GB> | --fixes <max routing table entries changed>]"
    exit(0)

  # Read command line arguments
  nettopo_f = sys.argv[1]
  rtable_f = sys.argv[2]
  loads_f = sys.argv[3]
  new_rtable_f = sys.argv[4]

  if sys.argv[5] == "--load":
    load_cutoff = int(sys.argv[6])
  elif sys.argv[5] == "--fixes":
    num_fixes = int(sys.argv[6])

  # Configure parsing routines for appropriate file formats.
  parse_rtable_file = parsers.parse_rtable_dump_fts_file__lid
  parse_nettopo_file = parsers.parse_nettopo_history_file__lid
  parse_loads_file = parsers.parse_loads_file

  # Parse the topology and read traffic matrix file.
  rtable = parse_rtable_file(rtable_f)
  nodeinfo, connections = parse_nettopo_file(nettopo_f)
  tm = parse_loads_file(loads_f)
  print "Done parsing all input files."

  # Calculate the link loads for current routing table.
  linkloads = rt.calculate_loads(rtable, tm, nodeinfo, connections)[0]
  print "Before AFAR: max link load is %.1f GB\n" % (max(linkloads.values()) / 1e9)

  # Run AFAR to get new routing table by link load or by iterations.
  if sys.argv[5] == "--load":
    newrtable = rt.get_AFAR_rtable_by_max_load(load_cutoff * int(1e9), 20, rtable, tm, nodeinfo, connections)
  elif sys.argv[5] == "--fixes":
    newrtable = rt.get_AFAR_rtable_by_iters(num_fixes, rtable, tm, nodeinfo, connections)

  # Write out the new routing table.
  parsers.write_rtable_dump_fts_file(newrtable, new_rtable_f)

  # Calculate the link loads for new routing table.
  newloads = rt.calculate_loads(newrtable, tm, nodeinfo, connections)[0]
  print "After AFAR: max link load is %.1f GB\n" % (max(newloads.values()) / 1e9)
