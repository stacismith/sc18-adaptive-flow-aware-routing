import sys
import json
import os


# Reads in routing table in the format of dump_fts output.
def parse_rtable_dump_fts_file__lid(rtable_f):
  global lid2guid
  global lid2name
  global name2lid
  lid2guid = {}
  lid2name = {}
  name2lid = {}

  # Load and build routing table.
  print "[parse_rtable_dump_fts_file__lid]"
  print "Constructing original routing table..."
  f = open(rtable_f, 'r')
  guid_rtable = {}
  guid2lid = {}

  # FORMAT:
  # Unicast lids [0x0-0x5db] of switch DR path slid 0; dlid 0; 0,1,19,19,3,18 guid 0x00066a00e300441b (ib36):
  #   Lid  Out   Destination
  #        Port     Info 
  # 0x0001 024 : (Channel Adapter portguid 0x001175000077281e: 'cab664 qib0')
  # 0x0002 000 : (Switch portguid 0x00066a00e300441b: 'ib36')
  # 0x0003 019 : (Switch portguid 0x00066a00ec0039f4: 'ibcore1 L110')
  # 0x0004 020 : (Switch portguid 0x00066a00ec003a45: 'ibcore1 L119')
  # ...
  # 0x05da 033 : (Channel Adapter portguid 0x001175000079253e: 'cab140 qib0')
  # 0x05db 036 : (Channel Adapter portguid 0x0011750000777f5e: 'cab1186 qib0')
  # 1417 valid lids dumped 
  switchid = ""
  for line in f:
    if line.startswith("Unicast lids"):
      split = line.split()
      ind = split.index("guid") + 1
      switchid = split[ind]
      guid_rtable[switchid] = {}

    if line.startswith("0x"):
      split = line.split()
      port = int(split[1])
      lid = int(split[0], 16)
      guid_rtable[switchid][str(lid)] = str(port)

      ind = split.index("portguid") + 1
      guid = split[ind][:-1]  # remove trailing :
      # ... '<name>')
      name = line.split("'")[1]
      if lid not in lid2guid:
        lid2guid[lid] = guid
      elif lid2guid[lid] != guid:
        print "WARNING: More than one guid recorded for lid %d (%s and %s)" % (lid, lid2guid[lid], guid)

      if lid not in lid2name:
        lid2name[lid] = name
        name2lid[name] = lid
      elif lid2name[lid] != name:
        print "WARNING: More than one name recorded for lid %d (%s and %s)" % (lid, lid2name[lid], name)

      if "'ib" in line:  # this is a switch, record guid -> lid
        if guid not in guid2lid:
          guid2lid[guid] = lid
        elif guid2lid[guid] != lid:
          print "WARNING: More than one lid recorded for guid %s (%d and %d)" % (guid, guid2lid[guid], lid)

  f.close()

  # Construct table with lid for switchid.
  rtable = {}
  for switchguid in guid_rtable:
    switchlid = guid2lid[switchguid]
    rtable[switchlid] = guid_rtable[switchguid]

  return rtable


# Writes out a routing table in the format of dump_fts, which is readable by OpenSM.
def write_rtable_dump_fts_file(newrtable, rtable_f):
  print "[write_rtable_dump_fts_file]"
  if os.path.exists(rtable_f):
    sys.stderr.write("ERROR: New routing table file already exists. New file not written.\n")
    return

  f = open(rtable_f, 'w')
  for switchid in newrtable:
    # Unicast lids [0x0-0x5db] of switch DR path slid 0; dlid 0; 0,1,19,19,3,18 guid 0x00066a00e300441b (ib36):
    f.write("Unicast lids [0x0-0x5db] of switch Lid %d guid %s ('%s'):\n" % (switchid, lid2guid[switchid], lid2name[switchid]))
    for lid in newrtable[switchid]:
      lidstr = format(int(lid), '04x')
      portstr = format(int(newrtable[switchid][lid]), '03d')
      if lid2name[int(lid)].startswith("cab"):
        desc = "Channel Adapter"
      elif lid2name[int(lid)].startswith("ib"):
        desc = "Switch"
      f.write("0x%s %s : (%s portguid %s: '%s')\n" % (lidstr, portstr, desc, lid2guid[int(lid)], lid2name[int(lid)]))
  f.close()


# Reads the network topology from OpenSM history file dump.
def parse_nettopo_history_file__lid(nettopo_f):
  # Load network topo file.
  print "[parse_nettopo_history_file__lid]"
  print "Loading network topology file..."
  f = open(nettopo_f, 'r')
  nettopo = json.load(f)
  f.close()

  # Build map from switch id -> lid.
  print "Extracting identifier information..."
  id2lid = {}
  id2name = {}
  for switchrec in nettopo['switches']:
    desc = switchrec['desc']
    switchid = switchrec['id']
    switchlid = switchrec['lid']
    id2lid[switchid] = switchlid
    id2name[switchid] = desc
  for noderec in nettopo['nodes']:
    desc = noderec['desc']
    nodeid = noderec['id']
    nodelid = noderec['lid']
    id2lid[nodeid] = nodelid
    id2name[nodeid] = desc

  # Build map from hostname (cabXXX) -> (lid in rtable, parent switch id)
  print "Extracting host-level information..."
  nodeinfo = {}
  for noderec in nettopo['nodes']:
    desc = noderec['desc']
    hostname = desc.split()[0]
    lid = name2lid[desc]
    if lid != noderec['lid']:
      print "WARNING! Mismatched lid: %s (%d, %d)" % (desc, lid, noderec['lid'])
    parentid = noderec['ports'][0]['dest_node']
    # Need lid here.
    parentlid = name2lid[id2name[parentid]]
    if parentlid != id2lid[parentid]:
      print "WARNING! Mismatched lid: %s (%d, %d)" % (id2name[parentid], parentlid, id2lid[parentid])
    nodeinfo[hostname] = {'lid': lid, 'switch': parentlid}

  # Build map from (switch id, port) -> destination switch id
  print "Extracting topology..."
  connections = {}
  for switchrec in nettopo['switches']:
    switchdesc = switchrec['desc']
    switchlid = name2lid[switchdesc]
    if switchlid != switchrec['lid']:
      print "WARNING! Mismatched lid: %s (%d, %d)" % (switchdesc, switchlid, switchrec['lid'])
    ports = switchrec['ports']
    for portrec in ports:
      port = portrec['num']
      destid = portrec['dest_node']
      # Need lid here.
      if destid != '':
        destlid = name2lid[id2name[destid]]
        if destlid != id2lid[destid]:
          print "WARNING! Mismatched lid: %s (%d, %d)" % (id2name[destid], destlid, id2lid[destid])
        destport = portrec['dest_port']
        connections[(switchlid, str(port))] = (destlid, str(destport))

  return nodeinfo, connections


# Reads in a traffic matrix file.
def parse_loads_file(loads_f):
  # Read in the file of loads of pairs of nodes.
  print "[parse_loads_file]"
  print "Reading in loads from placement..."
  tm = []

  # Format:
  # // APP srchost dsthost stream_weight
  # BIS cab259 cab824 18874368000
  # BIS cab838 cab260 18874368000
  # BIS cab109 cab705 18874368000
  # ...
  f = open(loads_f, 'r')
  for line in f:
    src, dest, weight = line.split()[1:4]
    tm.append((src, dest, int(weight)))
  f.close()

  return tm
