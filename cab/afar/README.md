AFAR
===

Our implementation of AFAR is found in `routing_routines.py`. It requires a traffic matrix file, a network topology file output by OpenSM, and a routing table in the format of the `dump_fts` command.

### Traffic Matrix
The traffic matrix file format is one node pair per line. Only non-zero entries are necessary. The format per line is `<application name> <src hostname> <dest hostname> <stream weight (bytes)>`.

For example, our bisection bandwidth application sends about 18.8 GB per node pair throughout the program, and the first lines of the traffic matrix file for one experiment are:

```
BIS cab259 cab824 18874368000
BIS cab838 cab260 18874368000
BIS cab109 cab705 18874368000
```

Host names should exactly match host names in the topology file.

### Examples
Example files for our experiments on Cab are in the `examples` subdirectory. To run a test:

```
cd examples
python driver-cab.py network.topo cab-ftree.lft streams/16job-random2-streams.tm afar.lft --load 65
```

The AFAR algorithm will read in Cab's topology (`network.topo`) and default routing tables (`cab-ftree.lft`), along with our traffic matrix for the random 2 placement of 16 jobs (`16job-random2-streams.tm`). The algorithm runs until a maximum link load of 65 GB is achieved (`--load 65`), and the new routing table is output to `afar.lft`.
