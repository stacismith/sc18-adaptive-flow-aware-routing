// FFT Test based on a Letterbox simulation of the 50-degree outer beam
// -----------------------------------------------------------------

include, Y_LAUNCH+"ydriver.i";

if (is_void(hyinit_ext) ) {
    error, "wrong code version -- need EXT coding";
}

dir_per_proc= 1;
num_sub_dirs= 32;

is_restart=0;

if(is_void(use_bg)) use_bg=0;

if(use_bg) {
  use_BG_io= 1;
} else {
  use_BG_io= 0;
}
/* choose between message passing and direct I/O */
imsgio= 1;
// if ismallmaster is set, skip variables in the master file that can be recomputed later
ismallmaster= 1;
/* choose between in memory files and one message or message
   per variable message passing dumps. */
use_memfiles= 1;

f3d_cachesize= 2^20;


// ------------------------------------------------------------------------------
run_id="bt_wb_06_16x8";

tcomment="\n \n 50o nx=1024, ny=1024, FFT TEST"

// ------------------------------ parallel filesystem options

pfs_topdir = get_env("PFS_TOPDIR");
if ( strlen(pfs_topdir) == 0 ) pfs_topdir="/p/lscratchd";

ipiofs=1;
pfsdir=pfs_topdir+"/"+get_env("LOGNAME")+"/pf3d/"+run_id;

if (!mp_rank) {
    mkdirp, pfsdir;
    write, "Writing to "+pfsdir;
}

// include files for parallelized beam input
include,Y_LAUNCH+"beam/mp_make_t00.i";
include,Y_LAUNCH+"beam/mp_setlbc.i";

focpt = [0.0,0.0,0.517834];
ang   = pi*50.0/180.;
zleh  = 0.503;     xleh = focpt(1) + (focpt(3)-zleh)*tan(ang);
z0    = zleh+0.11; x0   = focpt(1) + (focpt(3)-z0)*tan(ang);      // these are Lasnex units

sextent = 0.275/4;				// axial extent of 2.75 mm
nz_per_dom = 8;

sbegin =   sign(zleh-z0)*sqrt( (x0-xleh)^2 + (z0-zleh)^2 );    // s = 0 @ LEH
send   =   sbegin + sextent;     	// axial extent of sextent
sfoc   =   sign(zleh-focpt(3))*sqrt( (focpt(1) - xleh)^2 + (focpt(3) - zleh)^2 )

// ------------------------------ set processor parameters
mp_p = 16;  mp_q = 8;  mp_r = mp_size / (mp_p * mp_q);

// ------------------------------ grid size and resolution
tpi=2*pi;

nx = 1280;                      ny = 512;          // setting both nx and ny
delta_x = 4.0;                 delta_y = 4.0;     // setting both delta_x and delta_y
lx = nx * delta_x * tpi;       ly = ny * delta_y * tpi;

_slen= (send-sbegin)*1.e4/0.351;         // plasma length in wvl

nz= mp_r*nz_per_dom-1;
delta_z = _slen / (nz+1);
lz = delta_z * nz * tpi;


// ------------------------------ optional debugging control flags
nh3dbg=0; mfhdbg=0; lgtdbg=0; nprint=1; f3ddbg=1;

/* choose between default I/O groups and the special BG mapping */
if(use_BG_io) {
  mdio_user_setmasters= mdio_setmasters_bgl;
}

report_time_info=1;


// ------------------------------ usercode file
// - put functions called in "dousercode" here
if(imsgio) {
  if(use_memfiles) {
      if (!mp_rank) write,"Using in-memory file based viz, hist, and spec dumps";
    safe_include,Y_LAUNCH + "hist/yvizmem.i";
    safe_include,Y_LAUNCH + "hist/yspecmem.i";
  } else {
      if (!mp_rank) write,"Using message per variable based viz, hist, and spec dumps";
    safe_include,Y_LAUNCH + "hist/yvizmsg.i";
    safe_include,Y_LAUNCH + "hist/yspecmsg.i";
  }
  do_volvis= 0;
} else {
    if (!mp_rank) write,"Using file per process based viz, hist, and spec dumps";
  safe_include,Y_LAUNCH + "hist/yvizdmp.i";
  safe_include,Y_LAUNCH + "hist/yspecdmp.i";
}
do_volvis= 0;


// ------------------------------ other control switches
iloop = 2			 // select number of hydro steps per loop cycle
isubcycle = 50;                  // select subcycling at 25 light per hydro step
dt = 1.0;                         // about .25 ps
mhist = 2000;                     // allocate 'mhist' entries in history arrays

tstop=100.;                      // stop time in ps

// ------------------------------ visualization dumpfile options
use_mdio= 1;
mdio_group_size= 8;  // set number of processes per I/O group

domdsave= 0;          // enable multidomain restart files
skip_dumps= 1;

if(skip_dumps) {
  dohistdmp= 0;         // enable decomposed history dumps
  dospecdmp= 0;         // enable decomposed spectral history dumps
  dovizdmp= 0;          // enable visualization dumps
} else {
  dohistdmp= 1;         // enable decomposed history dumps
  dospecdmp= 1;         // enable decomposed spectral history dumps
  dovizdmp= 1;          // enable visualization dumps
}

ivizmaster=1;         // create master viz file for use with VisIt
use_bow= 0;           // if non-zero, use wavelet compression to save variables
vizfreq= 4;           // make viz dumps every vizfreq time steps
                      // Note that this is not a multiple of iloop steps
                      // as is the case for save files
ihistfreq = 7;        // make hist files every iloop cycles
ispecfreq= 2;        // how often to make decomposed spectral history dumps
		      // =2 for SRS, =10 for SBS 
shrink_req=[2,2,2];
//histdbg=1;
//specdbg=1;
if(skip_dumps) {
  dosave=0;             // file per cpu dump restarts on
} else {
  dosave=1;             // file per cpu dump restarts on
}
dump_strategy = 0;    // -2= special even/odd BGL dump mechamism
save_before_plot=0;

// ------------------------------ DoJob control variables
// NEW CONTROL VARIABLES --------------------------------------------------
report_time_freq=10;		 // N= timing info every N hydro steps
save_freq=14;			 // write re-start dumps ~ every 2.5ps
gfx_freq=0;			 // never write cgmfiles
how_many_dj= 2*save_freq;	 // how many total timesteps to run for the runtime
use_checkpoint=0;                // defaults to 1= checkpoint on


// ------------------------------ multifluid options
multifluid = 1;
input_ext = 0;
matlist =["CH", "H4He"];	// note:  for this run, Fracto+Fractch for CH
imap_curved=0;                    // 0= use X=0, 1= include azimuthal variation


// ------------------------------ light algorithm switches
// (controls allocation of storage)
ilgt=1;
isbs=0;                           // include SBS light  if non-zero
isrs=1;                           // include SRS light if non-zero
isrsf=0;                          // include SRSF light if non-zero
isrs4=0;                          // include SRS4 light if non-zero
ipol=1;			          // implement polarization smoothing
do_ssd=1;
ssd_bw=2.0;    	                  // change to 3.0 to add 3 angstroms


if (!multifluid) {
    izbc=5;                       // set up outflowing b.c.
} else {
    izbclo = izbchi = 3;	  // 3=outflowing b.c.s
    iybclo = iybchi = 3;  	  // 3=outflowing b.c.s
    ixbclo = ixbchi = 3*imap_curved;  	  // 0=periodic, 3=outflowing   b.c.s

}


if (isbs) {
    isbslocaldamping=1;           // calculate local SBS damping; (set before f3d_init)
    isbs_damptite=0;              // 0=do not use table to interpolate dampaw and freqaw
}

if (isrs||isrs4) {
    use_lwdrive_factor=1;             // 1=use raman couping cutoff (see scutoff below)
}

fft_msg = 2;                      // all-to-all communication
fft_alg=2;                        // yorick (Schwartztrauber) FFT

// ------------------------------ light parameters
// Set ilaser and tzfactor according to pPROP


ilaser  = 1.e15;		  // convenient normalization for whole beam simulation
harmonic=3;
tpeak = 10.0;                     // laser rise time (ps)
prepropagate_light=0;
wlength= wlength1/harmonic;


// ------------------------------ beam setup (inside timer)
                    _eltime = _eltime0 = array(double,3);
                    timer, _eltime0;

beamtype    = "cosbeam";
powmult     = 1.0;		// 100% intensity scaling of 50o beam at peak power
total_power = 6.907841e12;	// 7.0495 TW is power at 15.5 ns	
pcttran     = 1.00; 		// % transmitted to sbegin
pctltr      = 0.259;    	// % for nx=192 letterbox from ABL (compared to full beam)pctltr      *= double(lx/(384.*2*pi));  // % for nx=192 ABL letterbox (compared to full beam)
powfac      = powmult*pctltr*pcttran*total_power;

NIF_ywidth= 0.75 * ly/tpi;

include, Y_LAUNCH+"beam/mp_load_beam.i";

#if 0
// restore beam from file
if (!mp_rank) write, "Restoring beam from "+beam_name+"_BGL.pdb";
if (!grid_init_done) grid_init;     // ensure local processor variables are set
mxg0= mp_myp * nxl + 1;          mxg1= (mp_myp+1) * nxl;
myg0= mp_myq * nyl + 1;          myg1= (mp_myq+1) * nyl;

bglf=openb(beam_name+"_BGL.pdb");
restore, bglf, beam, beamps;
mp_pp= bglf.beam_pp_roll(mxg0:mxg1,myg0:myg1);
mp_ppps= bglf.beamps_pp_roll(mxg0:mxg1,myg0:myg1);
#endif

// _____________________________________

                    timer, _eltime;
                    if ( ! mp_rank ) timer_print, "beam setup", _eltime - _eltime0;


// ------------------------------ set up storage
f3d_init;


// Set up tzfactor
tzfactor_global = array(1.,nz+2);

// Add a feathering to tzfactor at entrance plane to ensure no density holes (RLB)
iznzl = min(10, nzl);

tzfactor_global(1:iznzl) *= exp(-iznzl+indgen(iznzl));
tzfactor(..) = tzfactor_global(nzg0:nzg1+1);


// ------------------------------ plasma parameters 
// Interpolate Lasnex cartesian mesh onto rotated pf3d mesh a la EAW's las2f3d.i

// -------------------- set up initial profiles
den0(..)   = 0.1;
te0(..)    = 2.5;
ti0(..)    =  1.0;
vx0(..)    =  0.0;
vy0(..)    =  0.0;
vz0(..)    =  0.0;
for (i=1; i<numberof(matlist); i++) psi0(i,..) = 1.0/numberof(matlist);


// ------------------------------ other switches
// landau damping: CH ~ .1, He ~ .2
ilandau=0;      damp0=0.1;        // ion wave k-dependent damping
                                  // turn off in transverse gradient runs; takes trans avg quantities
eeneqn=0;       itherm=1;
irelative=0;
ieabsorb=1;     ilangd=1;
ihysmth=1;

iequilibrate=0;                   // equilibrate ion and electron temps
ibeam_av=1;                       // use beam averaging to compute denav and teavg

ilw_noise_ixlim=nx+1;             // full width noise source in LW
ilw_noise_iylim=ny+1;

ilgt_advectz4=0;                  // use 6th order advection
ilw_splitstep=1;                  // use reordered LW with timesplit absorption
ilgt_splitstep=1;                 // use reordered splitstep light algorithm

if (ilgt_splitstep) isubcycle /= 2;

if (isbs && !isbslocaldamping) {
    s_sbsmatch=0.10;                  //(cm) chooses density for iaw freq/damping table
    isbs_z_damptite= int(s_sbsmatch/(send-sbegin)*nz);
    isbs_z_acsource= int(s_sbsmatch/(send-sbegin)*nz);
}

// for SRS matching 
denmatch=0.10; tematch=2.5;
//denmatch4=.14; tematch4=5.2;

// in future runs will use nonlinear threshhold multipliers
// isbs_lim=1;     isbs_nlthr_mult=0.5;
// isrslim=0;
//   isrslim=1;      isrs_nlthr_mult=0.25;

isrs_ne_limit = 0.22; isrs_ne_limit4 = 0.22;

if (isrs&&isrs4) {
   ne_cutover    = (denmatch+denmatch4)/2;
} else if (isrs) {
   ne_cutover    = isrs_ne_limit;
} else if (isrs4) {
   ne_cutover    = 0.02;
}

if (use_lwdrive_factor) {
   if (isrs)  set_raman_cutoff,  0.02,       ne_cutover;
   if (isrs4) set_raman_cutoff4, ne_cutover, isrs_ne_limit4;
}


// ------------------------------ retain old standard plotting mechanism
//include, get_env("HOME")+"/Filament/NEL/yplotNIF.i";   // special NIF graphics

xplanes = [ nx+1 ];
yplanes = [ ny+1 ];
zplanes = [2, nz/4, nz/2, 5*nz/8, 3*nz/4, 7*nz/8, nz];


// ------------------------------------------------------------------------------
// extra functions
// ------------------------------------------------------------------------------

std_ampt0 = ampt0;

func ampt0(tt)
/* DOCUMENT ampt0(tt)
       compute laser rise as before, but rescale from half-strength to full
       (instead of near-0 to full).
 */
{
    scale_fac= 0.25;
    return scale_fac + (1-scale_fac)*std_ampt0(tt);
}

// ------------------------------ end of deck
if ( ! mp_rank) {
    write,"mp_p= "+pr1(mp_p)+" mp_q= "+pr1(mp_q)+" mp_r= "+pr1(mp_r)+"\n";
    write,"could now say: step0; loop,_nloop_;";
}

