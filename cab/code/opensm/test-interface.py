#!/usr/bin/env python

from os import path, stat, kill
from subprocess import Popen, PIPE
from signal import SIGUSR2
from fcntl import lockf, LOCK_EX, LOCK_NB, LOCK_UN
from sys import exit
from datetime import datetime, timedelta


# CHANGE THIS FOLDER TO A FOLDER READABLE BY OPENSM CONTROL NODE
# AND USER.
routing_table_folder = path.join('/', 'home', 'username')
# CHANGE ROUTING TABLE FILE NAME HERE (OR NAME TABLE cab-routes.lft).
# FILE SHOULD BE IN FILE FORMAT OUTPUT BY dump_fts COMMAND.
osm_lfts_file = path.join(routing_table_folder, 'cab-routes.lft')


print '%s starting ...' % datetime.now().isoformat()

pid_file = 'job_aware_routing.pid'
fp_pid_file = open(path.join('/', 'dev', 'shm', pid_file), 'w')
try:
    lockf(fp_pid_file, LOCK_EX | LOCK_NB)
except IOError:
    exit('another instance is running')


# Check timestamp of routing file to see if a re-route should be triggered.
# Check that file exists first.
if not path.exists(osm_lfts_file):
    exit('routing table file doesn\'t exist')

file_mod_time = datetime.fromtimestamp(stat(osm_lfts_file).st_mtime)
now = datetime.today()

# CHANGE WINDOW TO MATCH FREQUENCY OF CRONTAB INSTALLATION FOR THIS SCRIPT
window = timedelta(minutes=1)

if now - file_mod_time < window:
    print 'Routing table file updated within last minute'

    # now we have to signal opensm to reroute the fabric with new routing tables
    try:
        osm_pid = Popen([path.join('/', 'sbin', 'pidof'), "opensm"], stdout=PIPE, stderr=PIPE).communicate()[0]
    except:
        exit('not able to get the pid of opensm')
    else:
        try:
            osm_pid = int(osm_pid)
        except:
            exit('no valid pid of opensm found')
        else:
            kill(osm_pid, SIGUSR2)

    print '%s rerouting scheduled' % datetime.now().isoformat()

else:
    print 'Routing table has not been updated'

print '%s exiting ...' % datetime.now().isoformat()

# cleaning up a bit
lockf(fp_pid_file, LOCK_UN)
fp_pid_file.close()

exit(0)
