#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <sys/time.h>

extern MPI_Comm appWorld;

double minTime = 1000000.0;
double maxTime = 0.0;
double sumTime = 0.0;
double compSumTime = 0.0;
int numExg = 0;
double startTime;
double compStartTime;
double stepStartTime;
struct timeval progStartTime;
struct timeval *syncTime;
int syncInd = 0;
int numSync;

void initProfile(int numPhases) {
  syncTime = malloc(sizeof(struct timeval) * numPhases);
  numSync = numPhases;
  gettimeofday(&progStartTime, NULL);
  stepStartTime = MPI_Wtime();
}

void startComp() {
  compStartTime = MPI_Wtime();
}

void endComp() {
  double time = MPI_Wtime() - compStartTime;
  compSumTime += time;
}

void startCommExchange() {
  startTime = MPI_Wtime();
}

void endCommExchange() {
  double time = MPI_Wtime() - startTime;
  if (time < minTime) {
    minTime = time;
  }
  if (time > maxTime) {
    maxTime = time;
  }
  sumTime += time;
  numExg += 1;
}

void enterSync() {
  gettimeofday(&syncTime[syncInd], NULL);
  syncInd += 1;
}

void exitSync(rank) {
  if (rank == 0) {
    double elapsed = MPI_Wtime() - stepStartTime;
    printf("Step %d %f\n", syncInd, elapsed);
  }
  stepStartTime = MPI_Wtime();
}

void writeProfile(int rank, int numranks) {
  // Max comm and comp times (not necessarily from same rank).
  double totalTime, compTotalTime;
  MPI_Reduce(&sumTime, &totalTime, 1, MPI_DOUBLE, MPI_MAX, 0, appWorld);
  MPI_Reduce(&compSumTime, &compTotalTime, 1, MPI_DOUBLE, MPI_MAX, 0, appWorld);
  if (rank == 0) {
    printf("Max comm exchange time: %f\n", totalTime);
    printf("Max comp time: %f\n", compTotalTime);
  }

  double *recvBuff = NULL;
  double sendBuff[3];
  if (rank == 0) {
    recvBuff = malloc(sizeof(double) * numranks * 3);
  }
  sendBuff[0] = minTime;
  sendBuff[1] = sumTime / numExg;
  sendBuff[2] = maxTime;

  MPI_Gather(sendBuff, 3, MPI_DOUBLE, recvBuff, 3, MPI_DOUBLE, 0, appWorld);

  long *syncTimes = NULL;
  if (rank == 0) {
    syncTimes = malloc(sizeof(long) * numranks * numSync);
  }
  long progStartMillis = progStartTime.tv_sec * 1000 + progStartTime.tv_usec / 1000;
  long *syncMillis = malloc(sizeof(long) * numSync);
  int i;
  for (i = 0; i < numSync; i++) {
    syncMillis[i] = syncTime[i].tv_sec * 1000 + syncTime[i].tv_usec / 1000 - progStartMillis;
  }

  MPI_Gather(syncMillis, numSync, MPI_LONG, syncTimes, numSync, MPI_LONG, 0, appWorld);

  if (rank == 0) {
    printf("Time statistics in seconds for comm exchanges\n");
    printf("Rank Min Avg Max\n");
    int i;
    for (i = 0; i < numranks; i++ ) {
      printf("%d ", i);
      int j;
      for (j = 0; j < 3; j++) {
        printf("%f ", recvBuff[i * 3 + j]);
      }
      printf("\n");
    }
    printf("Times in milliseconds, offset from start of %ld s %ld us\n", progStartTime.tv_sec, progStartTime.tv_usec);
    printf("Rank ");
    int j;
    for (j = 0; j < numSync; j++) {
      printf("Sync%d ", j);
    }
    printf("\n");
    for (i = 0; i < numranks; i ++) {
      printf("%d ", i);
      int j;
      for (j = 0; j < numSync; j++) {
        printf("%ld ", syncTimes[i * numSync + j]);
      }
      printf("\n");
    }
  }
}
