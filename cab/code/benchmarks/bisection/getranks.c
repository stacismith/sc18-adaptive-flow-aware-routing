
#include <stdio.h>
#include "mpi.h"

extern MPI_Comm appWorld;

/*
 * Returns the source and target that includes this node.
 * Assumes there is an even number of nodes.  The low ranks
 * are senders and the high ranks are receivers.
 */
int getranks(int rank, int *source, int *target, int *ppn)
{
  int ranks, half;

  MPI_Comm_size(appWorld, &ranks);
  if (ranks % 2 != 0) {
    printf("Even number of ranks required\n");
    MPI_Abort(appWorld, 1);
  }
  half = ranks / 2;

  *ppn = 0;
  if (rank < half) {
    *source = rank;
  }
  else {
    *source = rank - half;
  }
  *target = *source + half;

  return 0;
}
