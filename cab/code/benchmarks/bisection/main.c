/* 
   Argonne Leadership Computing Facility benchmark
   BlueGene/P version
   Bi-section bandwidth
   Written by Vitali Morozov <morozov@anl.gov>
   Modified by Daniel Faraj <faraja@us.ibm.com>
   
   Measures a partition minimal bi-section bandwidth
*/    
#define _GNU_SOURCE
#include <sched.h>
#include <string.h>

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

extern void do_sleep(long sleep);
extern void print_hosts();

#ifdef PROFILE
extern void initProfile(int numPhases);
extern void startComp();
extern void endComp();
extern void startCommExchange();
extern void endCommExchange();
extern void enterSync();
extern void exitSync(int rank);
extern void writeProfile(int rank, int numRanks);
#endif


MPI_Comm appWorld;

bisection_main( int argc, char *argv[], MPI_Comm comm )
{
  appWorld = comm;

  int ppn, taskid, ntasks, is1, ir1, i, k;
  char *sb, *rb;
  double d, d1;
  MPI_Status stat[2];
  MPI_Request req[2];
  double t1, elapsed, maxT, maxCommT;
 
  MPI_Comm_rank( appWorld, &taskid );
  MPI_Comm_size( appWorld, &ntasks );
   
  if (argc !=5) {
    if (taskid == 0) printf("Usage: %s <message size> <number of timesteps> <comm iters> <sleep nanoseconds>\n", argv[0]);
    MPI_Abort(appWorld, 1);
  }
  int msgSize, numPhases, iterations;
  long sleep;
  msgSize = atoi(argv[1]);
  numPhases = atoi(argv[2]);
  iterations = atoi(argv[3]);
  sleep = atoi(argv[4]);

  if (taskid == 0) {
    printf("Message size: %d, timesteps: %d, commiters: %d, computation: %ld nano\n", msgSize, numPhases, iterations, sleep);
    fflush(stdout);
  }

  posix_memalign( (void **)&sb, 16, sizeof( char ) * msgSize );
  posix_memalign( (void **)&rb, 16, sizeof( char ) * msgSize );

  if ( getranks( taskid, &is1, &ir1, &ppn ) == 0 )
  {
    d = 0.0;
    elapsed = 0.0;

    MPI_Barrier(appWorld);
#ifdef PROFILE
    initProfile(numPhases);
#endif

    t1 = MPI_Wtime();
    for ( k = 0; k < numPhases * iterations; k++ )
    {  
      // "Computation" for the phase
      if (sleep) {
#ifdef PROFILE
        startComp();
#endif

        do_sleep(sleep);

#ifdef PROFILE
        endComp();
#endif
      }

#ifdef PROFILE
      startCommExchange();
#endif

      // Communication for the phase
      if ( taskid == is1 )
      {
        MPI_Isend( sb, msgSize, MPI_BYTE, ir1, is1, appWorld, &req[0] );
        MPI_Irecv( rb, msgSize, MPI_BYTE, ir1, ir1, appWorld, &req[1] );
        MPI_Waitall( 2, req, stat );
      }
      if ( taskid == ir1 )
      {
        MPI_Isend( sb, msgSize, MPI_BYTE, is1, ir1, appWorld, &req[0] );
        MPI_Irecv( rb, msgSize, MPI_BYTE, is1, is1, appWorld, &req[1] );
        MPI_Waitall( 2, req, stat );
      }

#ifdef PROFILE
      endCommExchange();
#endif

      // Synchronize every "iterations" timesteps.
      if ((k + 1) % iterations == 0) {
#ifdef PROFILE
        enterSync();
#endif
        MPI_Barrier(appWorld);
#ifdef PROFILE
        exitSync(taskid);
#endif
      }

    }

    t1 = MPI_Wtime() - t1;
    elapsed = t1;

    t1 /= (double)(numPhases * iterations);
    d = 2 * (double)msgSize / t1;
    
    MPI_Reduce( &d, &d1, 1, MPI_DOUBLE, MPI_SUM, 0, appWorld );
    MPI_Reduce( &elapsed, &maxT, 1, MPI_DOUBLE, MPI_MAX, 0, appWorld);
        
    if ( taskid == 0 ) {
      printf("PPN: %d Bisection BW(GB/s): %.2lf\n", ppn, d1 / 1e9 );
      printf("Execution time: %f\n", maxT);
    }

#ifdef PROFILE
    writeProfile(taskid, ntasks);
#endif
  }

  free( sb );
  free( rb );

  print_hosts();
  return 0;
}
