/*
 * Simple global synchronization benchmark.
 */

#define _GNU_SOURCE
#include <sched.h>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

extern void do_sleep(long sleep);
extern void print_hosts();

#ifdef PROFILE
extern void initProfile(int numPhases);
extern void startComp();
extern void endComp();
extern void startCommExchange();
extern void endCommExchange();
extern void enterSync();
extern void exitSync(int rank);
extern void writeProfile(int rank, int numRanks);
#endif

MPI_Comm appWorld;

int gs_main(int argc, char ** argv, MPI_Comm comm) {

  appWorld = comm;

  int rank, ranks;
  MPI_Comm_size(appWorld, &ranks);
  MPI_Comm_rank(appWorld, &rank);

  int msgSize, numPhases, iterations;
  long sleep;

  if (argc != 5) {
    if (rank == 0) printf("Usage: %s <message size> <number of iterations> <comm iters> <sleep nanoseconds>\n", argv[0]);
    MPI_Abort(appWorld, 1);
  }
  msgSize = atoi(argv[1]);
  numPhases = atoi(argv[2]);
  iterations = atoi(argv[3]);
  sleep = atol(argv[4]);

  if (rank == 0) {
    printf("Message size: %d, iterations: %d, commiters: %d, computation: %ld nano\n", msgSize, numPhases, iterations, sleep);
    fflush(stdout);
  }

  int sendBuf[msgSize * ranks], recvBuf[msgSize * ranks];

#ifdef PROFILE
  MPI_Barrier(appWorld);
  initProfile(numPhases);
#endif

  double startt = MPI_Wtime();
  int i;
  for (i = 0; i < numPhases * iterations; i++) {
    // "Computation" for the phase
    if (sleep) {
#ifdef PROFILE
      startComp();
#endif

      do_sleep(sleep);

#ifdef PROFILE
      endComp();
#endif
    }

#ifdef PROFILE
    startCommExchange();
#endif

    // Communication for the phase
    MPI_Alltoall(sendBuf, msgSize, MPI_INT, recvBuf, msgSize, MPI_INT, appWorld);

#ifdef PROFILE
    endCommExchange();
#endif

    // Synchronize every "iterations" timesteps.
    if ((i + 1) % iterations == 0) {
#ifdef PROFILE
      enterSync();
#endif
      MPI_Barrier(appWorld);
#ifdef PROFILE
      exitSync(rank);
#endif
    }
  }
  double endt = MPI_Wtime();

  // Gather the running times
  double myTime = endt - startt;
  double maxTime;
  MPI_Reduce(&myTime, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, appWorld);
  
  if (rank == 0) {
    printf("Execution time: %f\n", maxTime);
  }

#ifdef PROFILE
  writeProfile(rank, ranks);
#endif

  print_hosts();
  return 0;
}
