#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

// Function pointer typedef for main.
typedef int (*MainMethod)(int, char**, MPI_Comm);

// Prototypes for the main functions of the benchmarks.
int nn_main(int argc, char *argv[], MPI_Comm appWorld);
int gs_main(int argc, char *argv[], MPI_Comm appWorld);
int bisection_main(int argc, char *argv[], MPI_Comm appWorld);
int pairs_main(int argc, char *argv[], MPI_Comm appWorld);
int pf3d_main(int argc, char *argv[], MPI_Comm appWorld);

// Supported apps.
#define NN "nn"
#define GS "gs"
#define BS "bis"
#define PS "pairs"
#define PF3D "pf3d"

// Configuration for each rank.
MPI_Comm APP_WORLD;
MainMethod appMain;
int appArgc;
char **appArgv;
int appRun;
char *appOutfile;
int globalRank, commSize, localRank;

/*
 *  Sleep for the specified nanoseconds, using nanosleep.
 */
void do_sleep(long sleep) {
  struct timespec sleep_time;
  sleep_time.tv_sec = sleep / 1000000000;
  sleep_time.tv_nsec = sleep % 1000000000;  // In nanoseconds.
  nanosleep(&sleep_time, NULL);
}

/*
 *  Prints the hosts for all ranks in the sub-job.
 *
 *  Precondition: setupComms(...) must be invoked before this function.
 */
void print_hosts() {
  char hostname[1024];
  gethostname(hostname, 1024);

  int rank, size;
  MPI_Comm_rank(APP_WORLD, &rank);
  MPI_Comm_size(APP_WORLD, &size);

  char *recv = NULL;
  if (rank == 0) {
    recv = malloc(1024 * size);
  }

  MPI_Gather(hostname, 1024, MPI_CHAR, recv, 1024, MPI_CHAR, 0, APP_WORLD);

  if (rank == 0) {
    int i;
    for (i = 0; i < size; i++) {
      printf("Rank %d: %s\n", i, recv + 1024 * i);
    }
  }
}


/*
 *  Processes one line of the input config, which corresponds to the
 *  information for one sub-job.
 */
void processAppConfig(char *linebuf, char **exe, int *numArgs, char ***args, int *run, int *subcommNodes, char **outfile) {
  // Split config line on space.
  const char s[2] = " ";
  char *token;
   
  // Get the executable name.
  token = strtok(linebuf, s);
  *exe = strdup(token);
  // Get the number of ranks.
  token = strtok(NULL, s);
  *subcommNodes = atoi(token);
  // Get whether or not to run.
  token = strtok(NULL, s);
  *run = atoi(token);
  // Get argc.
  token = strtok(NULL, s);
  *numArgs = atoi(token);
  // Get output file name.
  token = strtok(NULL, s);
  *outfile = strdup(token);

  // Get rest of args.
  char **tmpArgs = (char **)(malloc(*numArgs * sizeof(char *)));
  token = strtok(NULL, s);
  int i = 0;
  while (token != NULL && i < *numArgs) {
    tmpArgs[i] = strdup(token);
    i++;
    token = strtok(NULL, s);
  }
  *args = tmpArgs;

  // Sanity check.
  if (i != *numArgs) {
    if (globalRank == 0) {
      fprintf(stderr, "Not enough arguments specified for app in config: %d for \"%s\"\n", i, *exe);
    }
    MPI_Finalize();
    exit(1);
  }
}


/*
 *  Translates a job type from the config to the main method function for that
 *  job.
 */
MainMethod getMainMethod(char *exe) {
  MainMethod main;
  if (strcmp(NN, exe) == 0) {
    main = &nn_main;
  }
  else if (strcmp(GS, exe) == 0) {
    main = &gs_main;
  }
  else if (strcmp(BS, exe) == 0) {
    main = &bisection_main;
  }
  else if (strcmp(PS, exe) == 0) {
    main = &pairs_main;
  }
  else if (strcmp(PF3D, exe) == 0) {
    main = &pf3d_main;
  }
  else {
    if (globalRank == 0) {
      fprintf(stderr, "Invalid exe name: \"%s\"\n", exe);
    }
    MPI_Finalize();
    exit(1);
  }

  return main;
}


/*
 *  Usage help message.
 */
void help(char *name) {
  printf("Usage: %s <config file>\n", name);
  printf("Executables: %s, %s, %s, %s, %s\n", NN, GS, BS, PS, PF3D);
}


/*
 *  Reads the entire config file into memory, to be parsed later in setupComms.
 *
 *  This function must be called before setupComms(...).
 */
char **loadConfigFile(char *filename, int *numJobs) {
  // Open the config file.
  FILE *f = fopen(filename, "r");

  // Get number of jobs from first line of config.
  size_t linesize = 0;
  char *linebuf = NULL;
  ssize_t linelen = 0;
  linelen = getline(&linebuf, &linesize, f);
  if (linelen > 0) {
    sscanf(linebuf, "%d\n", numJobs);
    free(linebuf);
    linebuf = NULL;
  }
  else {
    if (globalRank == 0) {
      fprintf(stderr, "Unable to read number of jobs from config\n");
    }
    MPI_Finalize();
    exit(1);
  }

  char **configLines = (char **)(malloc(*numJobs * sizeof(char *)));
  int i;
  for (i = 0; i < *numJobs; i++) { 
    // Read the line in the config.
    linelen = 0;
    linelen = getline(&linebuf, &linesize, f);
    
    if (linelen > 0) {
      linebuf[linelen - 1] = '\0';  // Strip newline character.
      configLines[i] = strdup(linebuf);
      free(linebuf);
      linebuf = NULL;
    }
    else {
      if (globalRank == 0) {
        fprintf(stderr, "Unable to read job %d from config\n", i);
      }
      MPI_Finalize();
      exit(1);
    }
  }

  fclose(f);

  return configLines;
}


/*
 *  Splits MPI_COMM_WORLD into subcommunicators for each sub-job. Sub-job
 *  mapping is handled here (either linear or to randomly-shuffled nodes). Also
 *  loads the configuration for the sub-job, redirects each sub-job's stdout
 *  to a different file, etc.
 *
 *  Precondition: loadConfigFile(...) must be invoked before this function.
 *
 *  After this function has completed, all sub-jobs are ready to be launched.
 */
void setupComms(char **config, int numJobs, int cpn, long seed) {
  // Shuffle nodes randomly unless seed is 0, in which case linear placement is
  // used.
  int numNodes = commSize / cpn;
  int *shuffledNodes = malloc(sizeof(int) * numNodes);
  int i;
  for (i = 0; i < numNodes; i++) {
    shuffledNodes[i] = i;
  }

  // Do the shuffling if seed is not zero.
  if (globalRank == 0 && seed != 0) {
    // Set seed for random generator.
    srand48(seed);

    // Do the shuffling of nodes.
    int n = numNodes;
    int i;
    for (i = n - 1; i > 0; i--) {
      int j = drand48() * (i + 1);
      int t = shuffledNodes[j];
      shuffledNodes[j] = shuffledNodes[i];
      shuffledNodes[i] = t;
    }

    fprintf(stderr, "main: Random node mapping:\n");
    for (i = 0; i < numNodes; i++) {
      fprintf(stderr, "%d ", shuffledNodes[i]);
    }
    fprintf(stderr, "\n");

  }
  // Distribute order to all ranks.
  MPI_Bcast(shuffledNodes, numNodes, MPI_INT, 0, MPI_COMM_WORLD);

  // Used for splitting into different communicators for each jobid.
  int splitter[commSize];
  int totalnodes = 0;

  int jobid;
  for (jobid = 0; jobid < numJobs; jobid++) {
    // Get number of ranks, argc and argv, and whether to run from the config.
    int numArgs;
    char **args;
    int run;
    int subcommNodes;
    char *exe;
    char *outfile;
    processAppConfig(config[jobid], &exe, &numArgs, &args, &run, &subcommNodes, &outfile);

    // Extract which program to run.
    MainMethod main = getMainMethod(exe);

    // Add jobid for comm splitting, using node mapping array.
    int i;
    for (i = 0; i < subcommNodes; i++) {
      int nextNode = shuffledNodes[totalnodes];
      int j;
      for (j = 0; j < cpn; j++) {
        splitter[nextNode * cpn + j] = jobid;
      }
      // Set params for current rank.
      if (globalRank / cpn == nextNode) {
        appMain = main;
        appArgc = numArgs;
        appArgv = args;
        appRun = run;
        appOutfile = outfile;
      }
      totalnodes++;
    }
  }

#ifdef DEBUG
  if (globalRank == 0) {
    int i;
    fprintf(stderr, "main: MPI_Comm_split input:\n");
    for (i = 0; i < commSize; i++) {
      fprintf(stderr, "%d ", splitter[i]);
    }
    fprintf(stderr, "\n");
  }
#endif

  // Sanity check.
  if (totalnodes * cpn != commSize) {
    if (globalRank == 0) {
      fprintf(stderr, "Number of ranks for all jobs doesn't add up: %d out of %d\n", totalnodes * cpn, commSize);
    }
    MPI_Finalize();
    exit(1);
  }

  // Do the communicator split.
  MPI_Comm_split(MPI_COMM_WORLD, splitter[globalRank], 1, &APP_WORLD);
  MPI_Comm_rank(APP_WORLD, &localRank);

  // Redirect stdout to specified file.
  if (appRun) {
    if (localRank == 0) {
      FILE *result = freopen(appOutfile, "w", stdout);
      if (result == NULL) {
        fprintf(stderr, "Unable to redirect stdout to file: \"%s\"\n %s\n", appOutfile, strerror(errno));
        MPI_Finalize();
        exit(1);
      }
      fprintf(stderr, "Redirecting output for %s to \"%s\"\n", appArgv[0], appOutfile);
    }
  }
}


int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);

  // Initialization of MPI stuff.
  MPI_Comm_size(MPI_COMM_WORLD, &commSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &globalRank);

  // Print help message if requested.
  if (strcmp("-h", argv[1]) == 0) {
    if (globalRank == 0) {
      help(argv[0]);
    }
    MPI_Finalize();
    return 0;
  }

  // Read in the config file.
  int numJobs;
  char **config = loadConfigFile(argv[1], &numJobs);
  int cpn = atoi(argv[2]);
  long seed = atol(argv[3]);

  // Split the communicator and set up parameters for each rank / sub-job.
  setupComms(config, numJobs, cpn, seed);

  // Run the requested sub-jobs. Some sub-jobs in config can have appRun=FALSE,
  // in which case the job does not execute.
  int retval = 0;
  if (appRun) {
    if (localRank == 0) {
      time_t curtime;
      time(&curtime);
      fprintf(stderr, "Launched %s %s", appArgv[0], ctime(&curtime));
    }

    // Invoke sub-job main.
    retval = (*appMain)(appArgc, appArgv, APP_WORLD);

    if (localRank == 0) {
      time_t curtime;
      time(&curtime);
      fprintf(stderr, "Completed %s %s", appArgv[0], ctime(&curtime));
    }
  }

  // Wait for all sub-jobs to finish.
  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Finalize();
  return retval;
}

