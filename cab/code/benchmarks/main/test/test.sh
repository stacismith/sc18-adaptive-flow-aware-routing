#!/bin/bash

#SBATCH -p pdebug
#SBATCH -N 4
#SBATCH -t 00:05:00

srun -N 4 -n 64 ../main test.config 16 0

srun -N 2 -n 32 ../../bisection/bisection 64000 10 100 100000 > bis-2-solo.out
srun -N 2 -n 32 ../../nn/nn 16 64000 10 100 100000 > nn-2-solo.out
