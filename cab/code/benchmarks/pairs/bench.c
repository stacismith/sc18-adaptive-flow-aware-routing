#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <sys/time.h>
#include <unistd.h>


extern void do_sleep(long sleep);
extern void print_hosts();

#ifdef PROFILE
extern void initProfile(int numPhases);
extern void startComp();
extern void endComp();
extern void startCommExchange();
extern void endCommExchange();
extern void enterSync();
extern void exitSync(int rank);
extern void writeProfile(int rank, int numRanks);
#endif

MPI_Comm appWorld;

/*
 * Performs communication between N/2 random pairs of nodes.
 */
void p2p(MPI_Comm comm, int numprocs, int rank, int msgSize, int num_time_steps, int iterations, long sleep) {
  // Determine "random" partners first.
  int *partners = malloc(numprocs / 2 * sizeof(int));
  if (rank == 0) {
    int i;
    for (i = 0; i < numprocs / 2; i++) {
      partners[i] = numprocs / 2 + i;
    }

    srand(1331);
    for (i = 0; i < 2 * numprocs; i++) {
      int index1 = rand() % (numprocs / 2);
      int index2 = rand() % (numprocs / 2);
      int temp = partners[index1];
      partners[index1] = partners[index2];
      partners[index2] = temp;
    }
  }
  MPI_Bcast(partners, numprocs / 2, MPI_INT, 0, comm);

  // Allocate message space and send the messages.
  int *sendBuf = malloc(msgSize);
  int *recvBuf = malloc(msgSize);

#ifdef PROFILE
  MPI_Barrier(comm);
  initProfile(num_time_steps);
#endif

  // Start timing
  double startt = MPI_Wtime();
  int i;
  for(i = 0; i < num_time_steps * iterations; i++){
    // "Computation" for the timestep
    if (sleep) {
#ifdef PROFILE
      startComp();
#endif

      do_sleep(sleep);

#ifdef PROFILE
      endComp();
#endif
    }

#ifdef PROFILE
    startCommExchange();
#endif

    // perform communication
    int partner;
    if (rank < numprocs / 2) {
      partner = partners[rank];
      MPI_Send(sendBuf, msgSize, MPI_BYTE, partner, 0, comm);
    }
    else {
      MPI_Recv(recvBuf, msgSize, MPI_BYTE, MPI_ANY_SOURCE, 0, comm, MPI_STATUS_IGNORE);
    }

#ifdef PROFILE
    endCommExchange();
#endif

    // Synchronize every "iterations" timesteps.
    if ((i + 1) % iterations == 0) {
#ifdef PROFILE
      enterSync();
#endif
      MPI_Barrier(comm);
#ifdef PROFILE
      exitSync(rank);
#endif
    }
  }

  // End timing, clean up, and report time
  double endt = MPI_Wtime();

  double myTime = endt - startt;
  double maxTime;
  MPI_Reduce(&myTime, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, comm);

  if (rank == 0) {
    printf("Execution time: %f\n", maxTime);
  }

#ifdef PROFILE
  writeProfile(rank, numprocs);
#endif

  free(sendBuf);
  free(recvBuf);
}


int pairs_main(int argc, char**argv, MPI_Comm comm){

  appWorld = comm;

  int num_ranks, rank;

  //Start MPI, get num_ranks
  MPI_Comm_size(appWorld, &num_ranks);
  if(num_ranks == 0){
    printf("MPI_Comm_size failure\n");
    exit(1);
  }

  //Get global rank
  MPI_Comm_rank(appWorld, &rank);

  if (argc != 5) {
    if (rank == 0) printf("Usage: %s <message size> <number of iterations> <comm iters> <sleep nanoseconds>\n", argv[0]);
    MPI_Abort(appWorld, 1);
  }

  int msgSize = atoi(argv[1]);
  int numPhases = atoi(argv[2]);
  int iterations = atoi(argv[3]);
  long sleep = atol(argv[4]);

  if (rank == 0) {
    printf("Message size: %d, iterations: %d, commiters: %d, computation: %ld nano\n", msgSize, numPhases, iterations, sleep);
    fflush(stdout);
  }

  p2p(appWorld, num_ranks, rank, msgSize, numPhases, iterations, sleep);

  print_hosts();
  return 0;
}

