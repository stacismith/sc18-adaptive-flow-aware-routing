/*
 * Simple 2D nearest neighbor benchmark
 *
 * To do:
 * -Time only the communication loop
 */


extern void do_sleep(long sleep);
extern void print_hosts();

#ifdef PROFILE
extern void initProfile(int numPhases);
extern void startComp();
extern void endComp();
extern void startCommExchange();
extern void endCommExchange();
extern void enterSync();
extern void exitSync(int rank);
extern void writeProfile(int rank, int numRanks);
#endif

#define _GNU_SOURCE
#include <sched.h>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#define TAG 0

void postRecv(int *, int, int, MPI_Request [], int *);
void postSend(int *, int, int, MPI_Request [], int *);

MPI_Comm appWorld;

int nn_main(int argc, char ** argv, MPI_Comm comm) {

appWorld = comm;

int rank, ranks;
MPI_Comm_size(appWorld, &ranks);
MPI_Comm_rank(appWorld, &rank);

if (argc != 6) {
  if (rank == 0) printf("Usage: %s <row size> <message size> <number of timesteps> <comm iters> <sleep nanoseconds>\n", argv[0]);
  MPI_Abort(appWorld, 1);
}

int rowSize, msgSize, numPhases, iterations;
long sleep;
rowSize = atoi(argv[1]);
msgSize = atoi(argv[2]);
numPhases = atoi(argv[3]);
iterations = atoi(argv[4]);
sleep = atol(argv[5]);

if (rank == 0) {
  printf("Row size: %d, message size: %d, timesteps: %d, commiters: %d, computation: %ld nano\n", rowSize, msgSize, numPhases, iterations, sleep);
  fflush(stdout);
}

// Calculate the size of the columns
if (ranks % rowSize != 0) {
  if (rank == 0) printf("Row size must divide evenly into the number of ranks.\n");
  MPI_Abort(appWorld, 2);
}
int colSize = ranks / rowSize;

// Since we are sending ints, adjust message size to convert from bytes.
msgSize = msgSize / sizeof(int);

// Find my neighbors
int myX, myY;
int leftN, topN, rightN, bottomN, neighbors;
leftN = topN = rightN = bottomN = -1;
neighbors = 0;
myX = rank % rowSize;
myY = rank / rowSize;
if (myX > 0) {
  leftN = rank - 1;
  ++neighbors;
}
if (myX < rowSize - 1) {
  rightN = rank + 1;
  ++neighbors;
}
if (myY > 0) {
  topN = rank - rowSize;
  ++neighbors;
}
if (myY < colSize - 1) {
  bottomN = rank + rowSize;
  ++neighbors;
}

// Communicate with neighbors
int leftSendBuf[msgSize], topSendBuf[msgSize], rightSendBuf[msgSize], bottomSendBuf[msgSize];
int leftRecvBuf[msgSize], topRecvBuf[msgSize], rightRecvBuf[msgSize], bottomRecvBuf[msgSize];
MPI_Request requests[neighbors * 2];

#ifdef PROFILE
MPI_Barrier(appWorld);
initProfile(numPhases);
#endif

#ifdef NN_PROFILE
int phases = 0;
int iters = 0;
int profiling = 0;
double startstep, endstep;

int numItersProfiled = rowSize + 1;
double *profiledTimes = malloc(numItersProfiled * sizeof(double));
#endif

double startt = MPI_Wtime();
int i;
for (i = 0; i < numPhases * iterations; ++i) {
  // "Computation" for the phase
  if (sleep) {
#ifdef PROFILE
    startComp();
#endif

    do_sleep(sleep);

#ifdef PROFILE
    endComp();
#endif
  }

#ifdef PROFILE
  startCommExchange();
#endif

#ifdef NN_PROFILE
  if (profiling) {
    startstep = MPI_Wtime();
  }
#endif

  // Communication
  int reqi = 0;

  postRecv(leftRecvBuf, msgSize, leftN, requests, &reqi);
  postRecv(topRecvBuf, msgSize, topN, requests, &reqi);
  postRecv(rightRecvBuf, msgSize, rightN, requests, &reqi);
  postRecv(bottomRecvBuf, msgSize, bottomN, requests, &reqi);

  postSend(leftSendBuf, msgSize, leftN, requests, &reqi);
  postSend(topSendBuf, msgSize, topN, requests, &reqi);
  postSend(rightSendBuf, msgSize, rightN, requests, &reqi);
  postSend(bottomSendBuf, msgSize, bottomN, requests, &reqi);

  MPI_Waitall(reqi, requests, MPI_STATUSES_IGNORE);

#ifdef NN_PROFILE
  if (profiling) {
    endstep = MPI_Wtime();
//    printf("Rank %d, iteration %d, iteration time: %f\n", rank, i, endstep - startstep);
    profiledTimes[iters] = endstep - startstep;
    ++iters;
    if (iters >= numItersProfiled) {
      profiling = 0;
    }
  }
#endif

#ifdef PROFILE
  endCommExchange();
#endif

  // Synchronize every "iterations" timesteps.
  if ((i + 1) % iterations == 0) {
#ifdef PROFILE
    enterSync();
#endif

    MPI_Barrier(appWorld);

#ifdef NN_PROFILE
    ++phases;
    if (phases == 2) {
      profiling = 1;
    }
#endif

#ifdef PROFILE
    exitSync(rank);
#endif
  }
}

// End app timing.
double endt = MPI_Wtime();
double myTime = endt - startt;

// Gather the running times
double maxTime;
MPI_Reduce(&myTime, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, appWorld);
if (rank == 0) {
  printf("Execution time: %f\n", maxTime);
}

#ifdef PROFILE
writeProfile(rank, ranks);
#endif

#ifdef NN_PROFILE
// Gather iter times to rank 0 and print them out.
double *allProfiledTimes;
if (rank == 0) {
  allProfiledTimes = malloc(ranks * numItersProfiled * sizeof(double));
  if (allProfiledTimes == NULL) {
    fprintf(stderr, "NN: error: could not malloc buff for profile times (%ld bytes)\n", ranks * numItersProfiled * sizeof(double));
    fflush(stderr);
  }
}
MPI_Gather(profiledTimes, numItersProfiled, MPI_DOUBLE, allProfiledTimes, numItersProfiled, MPI_DOUBLE, 0, appWorld);

if (rank == 0) {
  int r, j;
  for (r = 0; r < ranks; r++) {
    for (j = 0; j < numItersProfiled; j++) {
      printf("Rank %d, iteration %d, iteration time: %f\n", r, j, allProfiledTimes[r * numItersProfiled + j]);
    }
  }
}
#endif

print_hosts();
return 0;

}

void postRecv(int * buf, int size, int source, MPI_Request requests[], int * reqi) {
  if (source >= 0) {
    MPI_Irecv(buf, size, MPI_INT, source, TAG, appWorld, &requests[*reqi]);
    ++(*reqi);
  }
}

void postSend(int * buf, int size, int dest, MPI_Request requests[], int * reqi) {
  if (dest >= 0) {
    MPI_Isend(buf, size, MPI_INT, dest, TAG, appWorld, &requests[*reqi]);
    ++(*reqi);
  }
}
