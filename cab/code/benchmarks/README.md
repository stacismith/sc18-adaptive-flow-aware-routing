Benchmarks
===

### Compilation
* Compiled with mvapich2-intel-2.2 module

To compile the full benchmark suite, enter ``main`` and run ``make``. To compile any benchmark separately, enter the directory and run ``make``.

### Running

## Bisection
```
cd bisection
srun -N <nodes> -n <ranks> ./bisection <msg_size_bytes> <timesteps> <exchanges_per_step> <comp_per_exchange_nano>
```

## Nearest neighbors
* Row size must divide number of ranks.
```
cd nn
srun -N <nodes> -n <ranks> ./nn <row_size> <msg_size_bytes> <timesteps> <exchanges_per_step> <comp_per_exchange_nano>
```

## Random pairs
```
cd pairs
srun -N <nodes> -n <ranks> ./bench <msg_size_bytes> <timesteps> <exchanges_per_step> <comp_per_exchange_nano>
```

## FFT proxy
* X\*Y\*Z must equal number of ranks.
```
cd pf3d
srun -N <nodes> -n <ranks> ./many2many <x_dim> <y_dim> <z_dim> <msg_size_bytes> <timesteps> <exchanges_per_step> <comp_per_exchange_nano>
```

## Multi-job workload test
```
cd main
srun -N <nodes> -n <ranks> ./main <jobs_config_file> <cores_per_node> <placement_random_seed>
```

The config file format is:
```
<num_jobs>
<app_name> <num_nodes> <run_job=0,1> <app_argc> <app_output_file> [app_argv]
<app_name> <num_nodes> <run_job=0,1> <app_argc> <app_output_file> [app_argv]
...
```

Example test found in ``main/test``:
```
cd main/test
srun -N 4 -n 64 ../main test.config 16 0
```
