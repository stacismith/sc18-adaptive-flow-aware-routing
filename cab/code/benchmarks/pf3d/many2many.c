/** \file many2many.C
 *  Author: Nikhil Jain 
 *  Date Created: March 19th, 2013
 */

 
#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"


#define MP_X 0
#define MP_Y 1
#define MP_Z 2

extern void do_sleep(long sleep);
extern void print_hosts();

#ifdef PROFILE
extern void initProfile(int numPhases);
extern void startComp();
extern void endComp();
extern void startCommExchange();
extern void endCommExchange();
extern void enterSync();
extern void exitSync(int rank);
extern void writeProfile(int rank, int numRanks);
#endif

MPI_Comm appWorld;

int pf3d_main(int argc, char **argv, MPI_Comm comm)
{

  appWorld = comm;

  int i, myrank, numranks, groupsize;
  int dims[3] = {0, 0, 0};          
  int temp[3] = {0, 0, 0};          
  int coord[3] = {0, 0, 0};          
  int periods[3] = {1, 1, 1};
  double startTime, stopTime;

  MPI_Comm cartcomm, subcomm;

  MPI_Comm_rank(appWorld, &myrank);
  MPI_Comm_size(appWorld, &numranks);

  if (argc !=8) {
    if (myrank == 0) printf("Usage: %s <x> <y> <z> <message size> <number of timesteps> <comm iters> <sleep nanoseconds>\n", argv[0]);
    MPI_Abort(appWorld, 1);
  }
  int numPhases, iterations;
  long sleep;
  numPhases = atoi(argv[5]);
  iterations = atoi(argv[6]);
  sleep = atoi(argv[7]);

  dims[MP_X] = atoi(argv[1]);
  dims[MP_Y] = atoi(argv[2]);
  dims[MP_Z] = atoi(argv[3]);

  MPI_Dims_create(numranks, 3, dims);
  MPI_Cart_create(appWorld, 3, dims, periods, 1, &cartcomm);
  MPI_Cart_get(cartcomm, 3, dims, periods, coord);
  temp[MP_X] = 0; temp[MP_Y] = 1; temp[MP_Z] = 0;
  MPI_Cart_sub(cartcomm, temp, &subcomm);

  MPI_Comm_size(subcomm,&groupsize);
  int perrank = atoi(argv[4]);
  char *sendbuf = (char*)malloc(perrank*groupsize);
  char *recvbuf = (char*)malloc(perrank*groupsize);

  if (myrank == 0) {
    printf("Message size: %d, timesteps: %d, commiters: %d, computation: %ld nano, x: %d, y: %d, z: %d\n", perrank, numPhases, iterations, sleep, dims[MP_X], dims[MP_Y], dims[MP_Z]);
    fflush(stdout);
  }

  MPI_Barrier(cartcomm);
#ifdef PROFILE
  initProfile(numPhases);
#endif

  startTime = MPI_Wtime();

  for (i=0; i<numPhases*iterations; i++) {
    // "Computation" for the phase
    if (sleep) {
#ifdef PROFILE
      startComp();
#endif

      do_sleep(sleep);

#ifdef PROFILE
      endComp();
#endif
    }

#ifdef PROFILE
    startCommExchange();
#endif

    // Communication for the phase
    MPI_Alltoall(sendbuf, perrank, MPI_CHAR, recvbuf, perrank, MPI_CHAR, subcomm);

#ifdef PROFILE
    endCommExchange();
#endif

    if ((i + 1) % iterations == 0) {
#ifdef PROFILE
      enterSync();
#endif
      MPI_Barrier(appWorld);
#ifdef PROFILE
      exitSync(myrank);
#endif
    }

  }

  MPI_Barrier(cartcomm);
  stopTime = MPI_Wtime();

  if(myrank == 0) {
    printf("Completed %d iterations for subcom size %d, perrank %d\n", numPhases * iterations, groupsize, perrank);
    printf("Execution time: %f\n", stopTime - startTime);
  }

#ifdef PROFILE
  writeProfile(myrank, numranks);
#endif

  print_hosts();
  return 0;
}

