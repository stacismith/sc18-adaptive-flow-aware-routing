Compiling Production Applications
===

### MILC
* Code found at: http://www.nersc.gov/users/computational-systems/cori/nersc-8-procurement/trinity-nersc-8-rfp/nersc-8-trinity-benchmarks/milc
* Files provided here:
    * patch for per-step timing: 0001-MILC-Per-timestep-timing-for-2018-re-routing-work.patch
* Compiled with mvapich2-intel-2.2 module

```
# Download and unzip TrN8MILC7May30.tar. If necessary, rename folder to milc7.
cd milc7
patch -p1 < 0001-MILC-Per-timestep-timing-for-2018-re-routing-work.patch
use mvapich2-intel-2.2
cd ks_imp_dyn
make su3_rmd
```

### Qbox
* Code found at: https://github.com/LLNL/qball 
* Commit number: a33d0438dd83372d645d670bef7e0060c8933011
* Files provided here:
    * patch for per-step timing: 0001-Qbox-Per-timestep-timing-for-2018-re-routing-work.patch
* Compiled with mvapich2-intel-2.2 module
* Installed Xerces through Spack package manager (https://github.com/spack/spack); installed Scalapack from scratch (http://www.netlib.org/scalapack)

```
git clone https://github.com/LLNL/qball.git
cd qball
patch -p1 < 0001-Qbox-Per-timestep-timing-for-2018-re-routing-work.patch
use mvapich2-intel-2.2
autoreconf -i
./configure \
    --with-xerces-prefix=<path-to-spack>/opt/spack/linux-rhel6-x86_64/gcc-4.4.7/xerces-c-3.1.4-e2cckyvv3modqe6nqzzcmineaaxtyfcr \  # your installation folder may be named differently
    --with-fftw3-prefix=/usr/local/tools/mkl-11.3.2 \
    --with-lapack=<path-to-scalapack-install>/libscalapack.a \
    --with-blacs=<path-to-scalapack-install>/libscalapack.a
make
```

### pF3D
Source code confidential.

### Running
See instructions found with each application to run the code. Our inputs are provided in the ``cab/data`` directory of this repo.
