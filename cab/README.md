### code
This directory contains the source code, build directions, and general execution directions for our production applications, benchmarks, and OpenSM modifications.

---
### data
This directory contains the problem inputs and experiment outputs for all our experiments.

---
### scripts
This directory contains our experiment scripts and analysis scripts.

---
### afar
This directory contains our Python implementation of AFAR.
