AFAR -- Adaptive Flow-Aware Routing
===
This repository contains the code and data used for the paper "Mitigating Inter-Job Interference Using Adaptive Flow-Aware Routing", SC'18.

---
### Cab (fat-tree)
The Cab directory contains the following items as described in the paper:

* Custom benchmark code

* Experimental results on fat-tree 

* Implementation of the AFAR algorithm

* Detailed build instructions for production applications on Cab

* Input problems for production applications at all process counts

---
### Edison (dragonfly)
The Edison directory contains the following items as described in the paper:

* Experimental results on dragonfly

* Machine learning scripts and results

* Detailed build instructions for MILC on Edison

* Input problem for MILC

---
More information can be found in the README files in each subdirectory.
